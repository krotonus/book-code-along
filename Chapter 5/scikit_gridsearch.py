# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from sklearn.tree import DecisionTreeClassifier

df = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data", header = None)
#df.columns = ['ID', 'diagnosis', 'radius', 'texture', 'perimeter', 'area', 'smoothness', 'compactness', 'concavity', 'concave_points', 'symmetry', 'fractal_dim']
X = df.iloc[:, 2:].values
y = df.iloc[:, 1].values
le = LabelEncoder()
y = le.fit_transform(y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)
pipe_svc = Pipeline([('scl', StandardScaler()),
                     ('clf', SVC(random_state=1))])
param_range = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid = [{'clf__C':param_range,
               'clf__kernel':['linear']},
                {'clf__C':param_range,
                 'clf__gamma':param_range,
                 'clf__kernel':['rbf']}]

gs = GridSearchCV(estimator = pipe_svc, param_grid= param_grid, scoring = 'accuracy', cv = 10, n_jobs = 2)
gs = gs.fit(X_train, y_train)
print(gs.best_score_)
print(gs.best_params_)

clf = gs.best_estimator_
clf.fit(X_train, y_train)
print("Test accuracy = %.3f " % clf.score(X_test, y_test))

#Nested cross-validation to get unbiased estimate of accuracy if we were to tune the hyperparameters later
gs = GridSearchCV(estimator=pipe_svc, param_grid= param_grid, scoring = 'accuracy', cv = 10, n_jobs = 3)
scores = cross_val_score(gs, X, y, scoring = 'accuracy', cv =5)
print("CV accuracy = %.3f +/- %.3f"%(np.mean(scores), np.std(scores)))

#Comparing The cross-validation scores of SVM with that of Decision Trees
gs = GridSearchCV(estimator=DecisionTreeClassifier(random_state=1), param_grid= [{'max_depth' : [1,2,3,4,5,6,7,None]}], scoring = 'accuracy', cv = 5, n_jobs= 3)
scores = cross_val_score(gs, X, y, scoring = 'accuracy', cv =5)
print("CV accuracy = %.3f +/- %.3f"%(np.mean(scores), np.std(scores)))

#we see that SVMs perform better. So we can move forward tuning the hyperparameters of SVM.
