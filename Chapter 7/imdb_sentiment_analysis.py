# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import re
import os, pickle
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords

def preprocessor(text):
        text = re.sub('<[^>]*>','', text)
        emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
        text = (re.sub('[\W]+', ' ', text.lower()) + ' '.join(emoticons).replace('-', ''))
        return text
    
def tokenizer(text):
    return text.split(' ')
    
def tokenizer_porter(text):
    porter = PorterStemmer()
    return [porter.stem(word) for word in text.split()]

if __name__ == '__main__':
        
    df = pd.read_csv('movie_data.csv', encoding = 'utf-8')
    count = CountVectorizer()
    docs = np.array(['The sun is shining','The weather is sweet', 'The sun is sweet, the weather is shining and one and one is two.'])
    bag = count.fit_transform(docs)
    tfidf = TfidfTransformer(use_idf=True, norm = 'l2', smooth_idf=True)
    np.set_printoptions(precision = 2)
    print(tfidf.fit_transform(count.fit_transform(docs)).toarray())
    
    #examples
    preprocessor(df.loc[0, 'review'][-50:])
    preprocessor("</a>This :) is :( a test :-)!")
    
    df['review'] = df['review'].apply(preprocessor)
    stop = stopwords.words('English')
    #testing
    [w for w in tokenizer_porter('a runner likes running and runs a lot')[-10:] if w not in stop]
    
    X_train = df.loc[:25000, 'review'].values
    y_train = df.loc[:25000, 'sentiment'].values
    X_test = df.loc[25000:, 'review'].values
    y_test = df.loc[25000:, 'sentiment'].values
    
    tfidf = TfidfVectorizer(strip_accents=None, lowercase=False, preprocessor=None)
    param_grid = [{'vect__ngram_range': [(1,1)],
                   'vect__stop_words': [stop, None],
                   'vect__tokenizer': [tokenizer, tokenizer_porter],
                   'clf__penalty': ['l1', 'l2'],
                   'clf__C': [1.0, 10.0, 100.0]},
                {'vect__ngram_range': ([1,1]),
                 'vect__stop_words': [stop, None],
                 'vect__tokenizer': [tokenizer, tokenizer_porter],
                 'vect__use_idf': [False],
                 'vect__norm': [None],
                 'clf__penalty': ['l1', 'l2'],
                 'clf__C': [1.0,10.0,100.0]}]
    lr_tfidf = Pipeline([('vect', tfidf),
                         ('clf', LogisticRegression(random_state=0))])
    
    gs_lr_tfidf = GridSearchCV(estimator=lr_tfidf, param_grid= param_grid, scoring='accuracy', cv = 5, verbose=1, n_jobs= 3)
    gs_lr_tfidf.fit(X_train, y_train)
    clf = gs_lr_tfidf.best_estimator_
    
    dest = os.path.join('movieclassifier_test', 'pkl_objects')
    if not os.path.exists(dest):
        os.makedirs(dest)
    pickle.dump(stop,open(os.path.join(dest, 'stopwords.pkl'),'wb'),protocol=4)
    pickle.dump(clf,open(os.path.join(dest, 'classifier.pkl'), 'wb'),protocol=4)
    
    print("Best parameters : %s"%gs_lr_tfidf.best_params_)


