# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from matplotlib.colors import ListedColormap

class LogisticRegression(object):
    def __init__(self, eta = 0.01, n_iter = 50):
        self.eta = eta
        self.n_iter = n_iter
        
    def fit(self, X, y):
        self.w_ = np.zeros(1 + X.shape[1])
        self.cost_ = []
        
        for i in range(self.n_iter):
            net_input = self.net_input(X)
            output = self.activation(X)
            errors = (y - output)
            self.w_[1:] += self.eta * X.T.dot(errors)
            self.w_[0] += self.eta * errors.sum()
            
            #computing Logistic Cost
            cost = -y.dot(np.log(output)) - ((1 - y).dot(np.log(1 - output)))
            self.cost_.append(cost)
        return self
    
    def  net_input(self, X):
        return np.dot(X, self.w_[1:]) + self.w_[0]
    
    def predict(self, X):
        return np.where(self.activation(X) >= 0.5, 1, 0)

    def activation(self, X):
        z = self.net_input(X)
        sigmoid = 1.0/(1.0 + np.exp(-z))
        return sigmoid
    
def plot_decision_regions(X, y, classifier, resolution = 0.02):
    markers = ['s', 'x', 'o', '^', 'v']
    colors = ['red', 'blue', 'lightgreen', 'gray', 'cyan']
    cmap = ListedColormap(colors[ :len(np.unique(y))])
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1 , xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution), np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T )
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha = 0.2, cmap = cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x = X[y == cl, 0], y = X[y == cl, 1], alpha=0.8, c = cmap(idx), marker= markers[idx], label = cl)

    
iris = load_iris()
X, y = iris.data[:100, [0,2]], iris.target[:100]

X_std = np.copy(X)
X_std[: , 0] = (X[:, 0] - X[:, 0].mean()) / X[:, 0].std()
X_std[: , 1] = (X[:, 1] - X[:, 1].mean()) / X[:, 1].std()

lr = LogisticRegression(n_iter = 25, eta = 0.15)
lr.fit(X_std, y)

plot_decision_regions(X_std, y, classifier=lr)
plt.title("Logistic Regression - Gradient Descent")
plt.xlabel("Sepal Length [standardized]")
plt.ylabel("Petal Length [standardized]")
plt.legend(loc = 'upper left')
plt.tight_layout()

plt.show()

plt.plot(range(1, len(lr.cost_)+1), lr.cost_, marker = 'o')
plt.xlabel("Epochs")
plt.ylabel("Logistic Cost")

plt.tight_layout()
plt.show()


