# -*- coding: utf-8 -*-
from scipy.misc import comb
import math
import numpy as np
import matplotlib.pyplot as plt

def ensemble_error(n_classifier, error):
    k_start = int(math.ceil(n_classifier/ 2.0))
    probs = [comb(n_classifier, k) * (error**k) * (1-error)**(n_classifier - k) for k in range(k_start, n_classifier +1)]
    return sum(probs)

error_range = np.arange(0.0, 1.01, 0.01)
ens_error = [ensemble_error(n_classifier= 11, error = i) for i in error_range]
plt.plot(error_range, ens_error, label = 'Ensemble Error' , linewidth = 2)
plt.plot(error_range, error_range, linestyle = '--', label = 'Base error', linewidth = 2)
plt.xlabel('Base Error')
plt.ylabel('Base/Ensemble Error')
plt.legend(loc = 'best')
plt.grid(alpha = 0.5)
plt.show()
