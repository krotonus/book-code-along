# -*- coding: utf-8 -*-
import os
import struct
import numpy as np
import matplotlib.pyplot as plt

def load_mnist_func(path, kind = 'train'):
    label_paths = os.path.join(path,'%s-labels.idx1-ubyte'%kind)
    images_path = os.path.join(path, '%s-images.idx3-ubyte'%kind)
    
    with open(label_paths, 'rb') as lpath:
        magic, n = struct.unpack('>II',lpath.read(8))
        labels = np.fromfile(lpath, dtype = np.uint8)
        
    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack('>IIII', imgpath.read(16))
        images = np.fromfile(imgpath, dtype = np.uint8).reshape(len(labels), 784)
        images = ((images/255.0) - 0.5) * 2
            
    return images, labels

if __name__ == '__main__':
    X_train, y_train = load_mnist_func('', kind = 'train')
    X_test, y_test = load_mnist_func('', kind = 't10k')
    #visualizing the MNIST data
    
    fig, ax = plt.subplots(nrows = 2, ncols = 5, sharex = True, sharey = True)
    ax = ax.flatten()
    for i in range(10):
        img = X_train[y_train == i][0].reshape(28,28)
        ax[i].imshow(img, cmap = 'Greys')
        
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.tight_layout()
    plt.show()
    #Showing Variation in handwriting
    fig, ax = plt.subplots(nrows = 5, ncols = 5, sharex = True, sharey = True)
    ax = ax.flatten()
    for i in range(25):
        img = X_train[y_train == 7][i].reshape(28,28)
        ax[i].imshow(img, cmap = 'Greys')
        
    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.tight_layout()
    plt.show()
    
    #saving the arrays for quicker laoding and usage
    np.savez_compressed('mnist_scaled.npz', X_train = X_train, y_train = y_train, X_test = X_test, y_test = y_test)
