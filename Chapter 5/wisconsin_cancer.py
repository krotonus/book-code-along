# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import StratifiedKFold.
from sklearn.cross_validation import cross_val_score

df = pd.read_csv("https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data", header = None)
#df.columns = ['ID', 'diagnosis', 'radius', 'texture', 'perimeter', 'area', 'smoothness', 'compactness', 'concavity', 'concave_points', 'symmetry', 'fractal_dim']
X = df.iloc[:, 2:].values
y = df.iloc[:, 1].values
le = LabelEncoder()
y = le.fit_transform(y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 1)
pipe_lr = Pipeline([('scl', StandardScaler()), 
                    ('pca', PCA(n_components=2)),
                    ('clf', LogisticRegression(random_state = 1))])
pipe_lr.fit(X_train, y_train)
print("Test accuracy = %.3f" % pipe_lr.score(X_test, y_test))

kfold = StratifiedKFold(n_splits= 10, random_state= 1)
scores = []

for k, (train, test) in enumerate(kfold.split(X_train, y_train)):
    pipe_lr.fit(X_train[train], y_train[train]) 
    score = pipe_lr.score(X_train[test], y_train[test])
    scores.append(score)
    print("Fold : %s, Class dist. : %s, Accuracy: %.3f" %(k+1, np.bincount(y_train[train]), score))

print("CV accuracy: %.3f +/- %.3f"%(np.mean(scores), np.std(scores)))

score = cross_val_score(estimator=pipe_lr, X= X_train, y=y_train, cv=10, n_jobs = -1)
print("CV accuracy score = %s" % scores)
print("CV accuracy = %.3f +/- %.3f"% (np.mean(score), np.std(score)))

