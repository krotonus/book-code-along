# -*- coding: utf-8 -*-
import tensorflow.contrib.keras as keras
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os, struct

def load_mnist(path, kind='train'):
    label_paths = os.path.join(path, '%s-labels.idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images.idx3-ubyte' % kind)

    with open(label_paths, 'rb') as lpath:
        magic, n = struct.unpack('>II', lpath.read(8))
        labels = np.fromfile(lpath, dtype=np.uint8)

    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack('>IIII', imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)
        images = ((images / 255.0) - 0.5) * 2

    return images, labels
    
X_train, y_train = load_mnist(path='./mnist/', kind = 'train')
X_test, y_test = load_mnist(path='./mnist', kind='t10k')

mean_vals = np.mean(X_train, axis= 0)
std_val = np.std(X_train)

X_train_centered = (X_train - mean_vals)/std_val
X_test_centered = (X_test - mean_vals) / std_val

del X_train, X_test

tf.set_random_seed(123)
np.random.seed(123)

y_train_onehot = keras.utils.to_categorical(y_train)
print("First 3 labels : ", y_train[:3])

model =keras.models.Sequential()
model.add(keras.layers.Dense(units=50, input_dim = X_train_centered.shape[1], kernel_initializer='glorot_uniform', bias_initializer='zeros', activation='tanh'))
model.add(keras.layers.Dense(units=50, input_dim = 50,kernel_initializer='glorot_uniform', bias_initializer='zeros', activation='tanh'))
model.add(keras.layers.Dense(units=y_train_onehot.shape[1], input_dim = 50, kernel_initializer='glorot_uniform', bias_initializer='zeros', activation='softmax'))

sgd_optimizer = keras.optimizers.SGD(lr=0.001, decay=1e-7, momentum=0.9)
model.compile(optimizer=sgd_optimizer, loss='categorical_crossentropy')

history = model.fit(X_train_centered, y_train_onehot, batch_size=64, epochs=50, verbose=1, validation_split=0.1)
y_train_pred = model.predict_classes(X_train_centered, verbose=0)
correct_pred = np.sum(y_train == y_train_pred, axis=0)
train_acc = correct_pred / y_train.shape[0]

print("Training Accuracy = %.2f%%"% (train_acc*100))


