# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from io import StringIO
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
%matplotlib inline

csv_data = '''A,B,C,D
1.0,2.0,3.0,4.0
5.0,6.0,,8.0
0.0,11.0,12.0,'''
df = pd.read_csv(StringIO(csv_data))
imr= Imputer(missing_values='NaN', axis = 0, strategy='mean')
imr = imr.fit(df)
imputed_data = imr.transform(df.values)


df = pd.DataFrame([['green', 'M', 10.1, 'class1'],
                   ['red', 'L', 13.5, 'class2'],
                   ['blue', 'XL',15.3, 'class1']])
df.columns = ['color', 'size', 'price', 'class']


size_mapping = { 'M': 2,
                'L': 3,
                'XL': 4}
inverse_size_mapping = {k:v for v,k in size_mapping.items()}
df['size'] = df['size'].map(size_mapping)

class_mapping = {label : idx for idx, label in enumerate(np.unique(df['class'].values))}
inverse_class_mapping = {idx : label for label, idx in class_mapping.items()}

label_enc = LabelEncoder()
label_enc.fit(df['class'])
y = label_enc.transform(df['class'])
y = label_enc.inverse_transform(y)


df_wine = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data', header = None )
df_wine.columns = ['Class label', 'Alcohol','Malic acid', 'Ash','Alcalinity of ash', 'Magnesium','Total phenols', 'Flavanoids','Nonflavanoid phenols','Proanthocyanins','Color intensity', 'Hue','OD280/OD315 of diluted wines','Proline']
X, y = df_wine.iloc[:,1:].values,df_wine.iloc[:,0].values
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = 0.3, random_state = 0)
scaler = StandardScaler()
scaler.fit(X_train)
X_train_std = scaler.transform(X_train)
X_test_std = scaler.transform(X_test)
lr = LogisticRegression(penalty = 'l1', random_state = 0, C= 0.1)
lr.fit(X_train_std, y_train)
print("Training Accuracy = ", lr.score(X_train_std, y_train))
print("Testing Accuracy = ", lr.score(X_test_std, y_test))
#Accesing the weight matrix
print(lr.coef_)

#Plotting the regularization path

fig = plt.figure()
ax = plt.subplot(111)
colors = ['blue','green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'pink', 'lightgreen', 'lightblue','gray', 'indigo', 'orange']
weights, params = [], []

for c in range(-4,6):
    lr = LogisticRegression(penalty='l1', C=10**c, random_state = 0)
    lr.fit(X_train_std, y_train)
    weights.append(lr.coef_[1])
    params.append(10**c)
    
weights = np.array(weights)
for column, color in zip(range(weights.shape[1]), colors):
    plt.plot(params, weights[:, column], label = df_wine.columns[column+1], color = color)
plt.axhline(0, color = 'black', linestyle = '--', linewidth = 3)
plt.xlim([10**(-5), 10**5])
plt.ylabel('Weight Coefficient')
plt.xlabel('C')
plt.xscale('log')
plt.legend(loc = 'upper left')
ax.legend(loc = 'upper center', bbox_to_anchor = (1.38, 1.03), ncol = 1, fancybox = True)
plt.show()



